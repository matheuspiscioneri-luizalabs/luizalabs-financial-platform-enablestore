import React, { Component } from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Image, FlatList, Switch, Alert, ScrollView, Keyboard, ActivityIndicator, ToastAndroid } from 'react-native'
import TimerMixin from 'react-timer-mixin'
import { Header, Btn } from '../components'
import Colors from '../styles/Colors'
import api from '../utils/api'

const CLEAR_BUTTON = require('../images/icon-clear.png')
const ALL_ENABLED = 'ALL_ENABLED'
const ALL_DISABLED = 'ALL_DISABLED'
const SEARCH = 'SEARCH'

export default class Home extends Component {
    constructor(props) {
        super(props)

        this.state = {
            search: '',
            allStores: [],
            storesDisplayed: [],
            isRefreshing: false,
            loading: true,
            loadMore: false,
            pageStart: 0,
            pageEnd: 0,
            pageSize: 20
        }
    }

    componentDidMount() {
        this._getAll()
    }

    _getAll = async () => {
        let stores = []
        this.setState({ pageStart: 0, pageEnd: this.state.pageSize, loading: true})

        try {
            stores = await api.get("/branches")
        } catch (error) {
            ToastAndroid.show("Erro ao carregar filiais", ToastAndroid.SHORT)
            console.error(error)
        }

        if (stores.data) {
            this.totalStores = stores.data.meta.recordCount
            const allStores = stores.data.records
            const storesDisplayed = allStores.slice(0, this.state.pageSize)
            this.setState({allStores: allStores, storesDisplayed: storesDisplayed})
        }
        
        this.setState({ loading: false, isRefreshing: false })
    }

    _refresh() {
        this.setState({isRefreshing: true}, () => {
            this._clearSearch()
        })
    }

    _loadMore = () => {
        let pageStart = this.state.pageStart
        let pageEnd = this.state.pageEnd
        let moreStores = []

        if (pageEnd < this.totalStores) {
            this.setState({ loadMore: true})
            this.timer = TimerMixin.setInterval(() => {
                pageStart = pageStart + this.state.pageSize
                pageEnd = pageEnd + this.state.pageSize

                if (this.state.allStores){
                    moreStores = this.state.allStores.slice(pageStart, pageEnd)
                }
                const storesDisplayed = this.state.storesDisplayed.concat(moreStores)
                this.setState({ storesDisplayed: storesDisplayed, pageStart: pageStart, pageEnd: pageEnd, loadMore: false })
                TimerMixin.clearInterval(this.timer)
            }, 1500)
        }
    }

    _search = async (type) => {
        Keyboard.dismiss()
        this.setState({ loading: true, pageStart: 0, pageEnd: this.state.pageSize })

        let stores = []

        try {

            switch (type) {
                case ALL_ENABLED: 
                    stores = await api.get('/branches?enabled=true')
                    break
                case ALL_DISABLED:
                    stores = await api.get('/branches?enabled=false')
                    break
                case SEARCH:
                default:
                    stores = await api.get(`/branches?ids=${this.state.search}`)
                    break
            }
        } catch (error) {
            ToastAndroid.show("Erro ao buscar filiais", ToastAndroid.SHORT)
            console.error(error)
        }

        const allStores = stores.data.records
        let storesDisplayed = []
        if (allStores && allStores.length > 0) {
            storesDisplayed = allStores.slice(0, this.state.pageSize)
        }

        this.setState({ allStores: allStores, storesDisplayed: storesDisplayed, isRefreshing: false, loading: false })
    }

    _clearSearch() {
        this.setState({ search: "" })
        this._getAll()
    }

    _toggle = (storeId, storeStatus) => {
        Alert.alert(
            'Confirmação',
            `Deseja realmente ${storeStatus ? 'habilitar' : 'desabilitar'} a filial ${storeId}?`, 
            [{
                text: 'Cancelar',
                onPress: () => false,
                style: 'cancel'
            },
            {
                text: 'Confirmar',
                onPress: () => {
                    api.patch(`/branches/${storeId}`, {'enable': storeStatus})
                        .then(data => {
                            let stores = this.state.storesDisplayed.map(store => {
                                if (store.branch.id === data.data.records[0].branch.id) {
                                    store.enabled = storeStatus
                                }
                                return store
                            })
                            this.setState({storesDisplayed: stores})
                        })
                        .catch(err => {
                            ToastAndroid.show("Erro ao editar filial", ToastAndroid.SHORT)
                            console.error(err)
                        })
                }
            }]
        )
    }

    _toggleAll = (storeStatus) => {
        Alert.alert(
            'Confirmação',
            `Deseja realmente ${storeStatus ? 'habilitar' : 'desabilitar'} todas filiais?`, 
            [{
                text: 'Cancelar',
                onPress: () => false,
                style: 'cancel'
            },
            {
                text: 'Confirmar',
                onPress: () => {
                    this.setState({ loading: true })
                    api.patch('/branches', {'enable': storeStatus})
                        .then(data => {
                            let stores = this.state.allStores.map(store => {
                                store.enabled = storeStatus
                                return store
                            })

                            const storesDisplayed = stores.slice(this.state.pageStart, this.state.pageEnd)
                            this.setState({ allStores: stores, storesDisplayed: storesDisplayed, loading: false })
                        })
                        .catch(err => {
                            ToastAndroid.show("Erro ao editar filiais", ToastAndroid.SHORT)
                            console.error(err)
                        })
                }
            }]
        )
    }

    _onPopupEvent = (eventName, index) => {
        if (eventName !== 'itemSelected') return
        if (index === 0) this._search(ALL_ENABLED)
        if (index === 1) this._search(ALL_DISABLED)
        if (index === 2) this._toggleAll(true)
        if (index === 3) this._toggleAll(false)
        if (index === 4) this._refresh()
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{ alignItems: 'center', justifyContent: 'center', position: 'absolute', top: 0, right: 0, bottom: 0, left: 0, backgroundColor: Colors.WHITE, opacity: this.state.loading ? .5 : 0 }}>
                    <ActivityIndicator animating size="large" color={Colors.BLUE} style={{ alignItems: 'center', justifyContent: 'center' }} />
                </View>
                <View style={{ opacity: this.state.loading ? .5 : 1 }}>
                    <Header title="Habilitar filiais" onPopupEvent={(eventName, index) => this._onPopupEvent(eventName, index)} />
                    <View style={styles.content}>
                        <TextInput
                            style={styles.input}
                            underlineColorAndroid='transparent'
                            keyboardType='phone-pad'
                            placeholder="Insira o ID das filiais"
                            returnKeyType={'search'}
                            value={this.state.search}
                            onSubmitEditing={() => this._search(SEARCH)}
                            onChangeText={(text) => this.setState({ search: text })}
                        />
                        <TouchableOpacity
                            style={styles.icon}
                            accessibilityTraits="button"
                            onPress={() => this._clearSearch()}
                            activeOpacity={0.9}>
                            <Image source={CLEAR_BUTTON} />
                        </TouchableOpacity>
                    </View>
                    {this.state.storesDisplayed.length ?
                        <View style={styles.containerList}>
                            <FlatList
                                keyExtractor={item => item ? item.branch.id : ''}
                                data={this.state.storesDisplayed}
                                onRefresh={() => this._refresh()}
                                refreshing={this.state.isRefreshing}
                                onEndReachedThreshold={0.5}
                                onEndReached={(info) => this._loadMore()} 
                                renderItem={({ item }) => (
                                    <View style={styles.listItem}>
                                        <Text style={styles.listItemId}>
                                            {item ? item.branch.name : ''}
                                        </Text>
                                        <Switch
                                            style={styles.listItemStatus}
                                            onValueChange={(value) => this._toggle(item.branch.id, value)}
                                            value={item ? item.enabled : false} />
                                    </View>
                                )}
                            />                        
                        </View>
                    :
                        <Text style={[styles.textEmpty]}>Nenhuma registro encontrado...</Text>
                    }
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE,
        marginTop: 24
    },
    content: {
        padding: 10,
        backgroundColor: Colors.BLUE
    },
    input: {
        height: 38,
        fontSize: 16,
        color: Colors.DEFAULT,
        paddingRight: 40,
        backgroundColor: Colors.WHITE,

        borderRadius: 2,
        shadowOpacity: 0.75,
        shadowRadius: 5,
        shadowColor: 'red',
        shadowOffset: { height: 0, width: 0 },
    },
    icon: {
        backgroundColor: 'transparent',
        position: 'absolute',
        top: 12,
        right: 10,
        padding: 10,
    },
    containerList: {
        paddingHorizontal: 10,
        paddingBottom: 220
    },
    listItem: {
        paddingHorizontal: 5,
        paddingTop: 15,
        paddingBottom: 10,
        flexDirection: 'row',
        borderBottomColor: Colors.GRAY_LIGTH,
        borderBottomWidth: StyleSheet.hairlineWidth
    },
    listItemId: {
        flex: 1,
        alignSelf: 'flex-start'
    },
    listItemStatus: {
        flex: 1,
        alignSelf: 'flex-end'
    },
    buttonBox: {
        marginVertical: 30,
        flexDirection: 'row'
    },
    button: {
        flex: 1,
        marginHorizontal: 10,
    },
    textEmpty: {
        color: Colors.GRAY_LIGTH,
        padding: 10,
    },
})
