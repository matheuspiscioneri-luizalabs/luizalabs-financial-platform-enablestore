import React, { Component } from 'react'
import Home from './container/Home'

export default class App extends Component {
  componentWillMount() {
    console.disableYellowBox = true;
  }
  
  render() {
    return (
      <Home />
    )
  }
}