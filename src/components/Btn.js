import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';
import PropTypes from 'prop-types';

export class Btn extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <TouchableOpacity
        accessibilityTraits="button"
        onPress={this.props.onPress}
        disabled={this.props.disabled}
        activeOpacity={0.9}
        style={[styles.button, this.props.style, { backgroundColor: this.props.backgroundColor, borderColor: this.props.borderColor }]}>
        <Text style={[styles.textButton, { color: this.props.textColor }]}>{this.props.text}</Text>
      </TouchableOpacity>
    )
  }
}

Btn.propTypes = {
  onPress: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  style: PropTypes.any,
  backgroundColor: PropTypes.string.isRequired,
  borderColor: PropTypes.string.isRequired,
  textColor: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

const styles = StyleSheet.create({
  button: {
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 2,
    borderRadius: 3,
    marginBottom: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  textButton: {
    letterSpacing: 1,
    flex: 1,
    fontSize: 18,
    textAlign: 'center',
    backgroundColor: 'transparent'
  },
});
