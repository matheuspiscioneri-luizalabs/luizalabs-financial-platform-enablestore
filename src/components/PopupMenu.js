import React, { Component, PropTypes } from 'react'
import { View, UIManager, findNodeHandle, StyleSheet, Image, TouchableOpacity } from 'react-native'

const MORE_BUTTON = require('../images/icon-more.png')

export class PopupMenu extends Component {

    constructor(props) {
        super(props)
        this.state = {
            icon: null
        }
    }

    onError() {
        console.log('Popup Error')
    }

    onSucess(element, index) {
        console.log(`${element} : ${index}`)
    }

    onPress = () => {
        if (this.state.icon) {
            UIManager.showPopupMenu(
                findNodeHandle(this.state.icon),
                this.props.actions,
                this.onError,
                this.props.onPress
            )
        }
    }

    onRef = icon => {
        if (!this.state.icon) {
            this.setState({ icon })
        }
    }

    render() {
        return (
            <TouchableOpacity onPress={this.onPress}>
                <View style={styles.rightButton}>
                    <Image ref={this.onRef} source={MORE_BUTTON} />
                </View>
            </TouchableOpacity>
        )
    }

}

let styles = StyleSheet.create({
    rightButton: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: 32,
        paddingRight: 16
    }
})