import React, { Component } from 'react'
import { View, Image, Text, TouchableOpacity, StatusBar, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'

import { ColorBar, Btn, PopupMenu } from './'
import Colors from '../styles/Colors'

export class Header extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={Colors.BLUE_DARK} translucent barStyle="light-content" />
        <ColorBar />
        <View style={styles.menu}>
          <View style={styles.title}>
            <Text style={styles.titleText} numberOfLines={1}>
              {this.props.title}
            </Text>
          </View>
          <PopupMenu actions={['Ver habilitadas', 'Ver desabilitadas', 'Habilitar todas', 'Desabilitar todas', 'Atualizar']} onPress={this.props.onPopupEvent} />
        </View>
      </View>
    )
  }
}

Header.propTypes = {
  title: PropTypes.string.isRequired,
}

const styles = StyleSheet.create({
  container: {
    height: 50,
    backgroundColor: Colors.BLUE
  },
  menu: {
    flex: 1,
    flexDirection: 'row'
  },
  title: {
    flex: 1,
    justifyContent: 'center',
    overflow: 'hidden',
    paddingLeft: 16
  },
  titleText: {
    fontSize: 18,
    fontWeight: '400',
    color: Colors.WHITE
  }
})
