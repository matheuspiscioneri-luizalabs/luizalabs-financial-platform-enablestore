import React, { Component } from 'react'
import { View, Dimensions } from 'react-native'
import PropTypes from 'prop-types'
import Colors from '../styles/Colors'

export class ProgressBar extends Component {
  constructor(props) {
    super(props)

    const colorIndex = this.props.defaultColor === props.colors[0] ? 1 : 0

    this.state = {
      colors: [
        { id: props.colors[colorIndex], size: 1 },
        { id: props.defaultColor, size: 99 }
      ]
    }

    this._loading = true
    this._changeColors = this._changeColors.bind(this)
  }

  _changeColors() {
    let colors = this.state.colors
    let colorIndex = 0
    let firstSize = 1
    let lastSize = 99
    const limit = 100

    clearInterval(this._intervalId)

    this._intervalId = setInterval(() => {
      if (this._loading) {
        firstSize += 2
        lastSize -= 2

        colors[0].size = firstSize
        colors[1].size = lastSize

        if (firstSize >= limit) {
          colorIndex = ((colorIndex + 1) === this.props.colors.length) ? 0 : colorIndex + 1
          colors[1].id = colors[0].id

          let color = this.props.colors[colorIndex]
          if (color === this.props.defaultColor) {
            colorIndex = ((colorIndex + 1) === this.props.colors.length) ? 0 : colorIndex + 1
            color = this.props.colors[colorIndex]
          }

          colors[0].id = color

          firstSize = 1
          lastSize = 99

          colors[0].size = firstSize
          colors[1].size = lastSize
        }

        this.setState({ colors })
      } else {
        clearInterval(this._intervalId)
      }
    }, this.props.interval)
  }

  componentDidMount() {
    this._changeColors()
  }

  componentWillUnmount() {
    this._loading = false
  }

  render() {
    return (
      <View style={{ flexDirection: 'row' }}>
        <View
          style={{
            flex: this.state.colors[0].size,
            height: this.props.height,
            backgroundColor: this.state.colors[0].id
          }} />
        <View
          style={{
            flex: this.state.colors[1].size,
            height: this.props.height,
            backgroundColor: this.state.colors[1].id
          }} />
      </View>
    )
  }
}

ProgressBar.propTypes = {
  interval: PropTypes.number,
  height: PropTypes.number,
  defaultColor: PropTypes.string,
  colors: PropTypes.array
}

ProgressBar.defaultProps = {
  interval: 10,
  height: 4,
  defaultColor: Colors.BLUE,
  colors: [
    Colors.YELLOW,
    Colors.ORANGE,
    Colors.RED,
    Colors.PURPLE,
    Colors.DEEP_PURPLE,
    Colors.BLUE,
    Colors.CYAN,
    Colors.GREEN_LIGTH,
    Colors.GREEN
  ]
}