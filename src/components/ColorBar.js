import React, { Component } from 'react';
import { View } from 'react-native';

export class ColorBar extends Component {
  render() {
    return (
      <View style={{ flexDirection: 'row' }}>
        <View style={{ flex: 1, height: 4, backgroundColor: '#FFCE00' }} />
        <View style={{ flex: 1, height: 4, backgroundColor: '#FB9600' }} />
        <View style={{ flex: 1, height: 4, backgroundColor: '#E25335' }} />
        <View style={{ flex: 1, height: 4, backgroundColor: '#DE349E' }} />
        <View style={{ flex: 1, height: 4, backgroundColor: '#BF2FDC' }} />
        <View style={{ flex: 1, height: 4, backgroundColor: '#5D33D5' }} />
        <View style={{ flex: 1, height: 4, backgroundColor: '#2B7CD7' }} />
        <View style={{ flex: 1, height: 4, backgroundColor: '#33C3DD' }} />
        <View style={{ flex: 1, height: 4, backgroundColor: '#32D9A1' }} />
        <View style={{ flex: 1, height: 4, backgroundColor: '#3FCB2A' }} />
      </View>
    );
  }
}
