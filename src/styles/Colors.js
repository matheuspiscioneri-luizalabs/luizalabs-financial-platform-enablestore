const colors = {
  DEFAULT: '#404040',
  BLUE: '#0083ca',
  BLUE_DARK: '#0068a1',
  BLUE_LIGTH: '#5295cc',
  GREEN: '#3FCB2A',
  YELLOW: '#ffce00',
  ORANGE: '#FB9600',
  RED: '#e25335',
  GRAY: '#8c8c8c',
  GRAY_LIGTH: '#D7D7D7',
  GRAY_LIGTH_2: '#EEEEEE',
  GRAY_LIGHTEST: '#F4F4F4',
  BLACK: '#000000',
  WHITE: '#ffffff',
  PURPLE: '#BF2FDC',
  DEEP_PURPLE: '#5c33d5',
  ROSE: '#DE349E',
  CYAN: '#33C3DD',
  GREEN_LIGTH: '#32D9A1',
  GREEN: '#3FCB2A'
};

export default colors;