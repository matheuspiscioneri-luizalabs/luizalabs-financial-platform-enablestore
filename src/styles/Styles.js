import { StyleSheet } from 'react-native';

import Colors from './Colors';

const commonStyles = StyleSheet.create({
  pageContainer: {
    flex: 1,
    backgroundColor: Colors.WHITE,
    marginTop: 24
  },
});

export default commonStyles;