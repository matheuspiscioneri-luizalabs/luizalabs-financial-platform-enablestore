import Config from 'react-native-config';

const config = {
    API_URL: Config.API_URL,
    API_KEY: Config.API_KEY,
};

export default config; 